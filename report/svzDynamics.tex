\documentclass[a4paper,12pt]{scrartcl}
\input{input/packages.tex}
\input{input/definitions.tex}

\title{Modeling the Dynamics\\ of Neural Stem Cells\\ in the Subventricular Zone}

\begin{document}

\maketitle

\section{Model Definition}
We aim to set-up and analyze a basic model of cells in the subventricular zone (SVZ), capable of reproducing observed experimental data. The model is comprised of neural stem cells (NSCs) and progenitor cells, since these are the two cell types being analyzed in the data. The dynamics of NSCs and progenitors have been experimentally addressed before, suggesting that
NSCs exist in a quiescent or active (dividing) state \autocite{Ponti:2013,Calzolari:2015,Lim:2016}. Dividing NSCs are suggested to produce progenitors via asymmetric divisions \autocite{Ponti:2013,Lim:2016} and to undergo asymmetric divisions as well as symmetric self-renewing divisions \autocite{Calzolari:2015}. Moreover, progenitors have been suggested to undergo 2-3 rounds of symmetric divisions \autocite{Ponti:2013,Calzolari:2015,Lim:2016}.

We thus assume a model of NSCs in the SVZ, in which quiescent NSCs (qNSCs) can enter the cell cycle to become activated NSCs (aNSCs; Figure \ref{eq:model}). Activated NSCs divide and become either two qNSCs or two progenitors with the capacity of performing $n$ symmetric self-renewing divisions. 

The reason for assuming the two division modes of stem cells is that firstly, the model needs to explain the age-related decline of NSC numbers, which has been observed in the data as well as in previous studies \autocite{Shook:2012,Capilla:2014,Silva:2016} and secondly, that NSCs that once incorporated BrdU become quiescent. Assuming the two stated division modes can explain both observations. It is also possible to explain both observations if instead of the divisions of the kind $\text{aNSC}\rightarrow 2\times\text{qNSC}$, division of the form $\text{aNSC}\rightarrow \text{progenitor}+\text{qNSC}$ are assumed. However, it can be shown that both models are equivalent and lead to same conclusions.

Taken together, our model is given by the set of equations
\begin{equation}
\label{eq:model}
\begin{aligned}
\ddt \, \textsf{qNSC}(t) &=-r\textsf{qNSC}(t) + 2 b p^\text{stem}\textsf{aNSC}(t),\\
\ddt \, \textsf{aNSC}(t) &= r\,\textsf{qNSC}(t) - p^\text{stem} \textsf{aNSC}(t),\\
\ddt \,\textsf{prog}_n(t) &= -p^\text{prog} \,\textsf{prog}_n(t)+2(1-b)p^\text{stem}\,\textsf{qNSC}(t),\\
\ddt \,\textsf{prog}_k(t) &= 2p^\text{prog}\textsf{prog}_{k+1}(t)-p^\text{prog}\textsf{prog}_k(t),\\
\end{aligned}
\end{equation}
$k\in\{0,\ldots,n-1\}$, where $r$ is the activation rate of qNSCs, $p^\text{stem}$ the division rate of NSCs, $b$ the probability of an aNSC to divide into two qNSCs versus to progenitors. The parameter $p^\text{prog}$ denotes the division rate of progenitors and $n\in\N$ the total number of division rounds of a progenitor, and $t$ is the age of the animal. 

Since we solely aim to investigate the dynamics of NSCs, we follow a top-down approach in which we start with a simplistic model which is capable of reconstructing the produced data. Thus, instead of following the suggestion of \textcite{Ponti:2013}, that progenitors perform $3$ symmetric self-renewing divisions followed by direct transformation into neuroblasts, we assume that progenitors undergo $n=2$ symmetric self-renewing divisions followed by symmetric division into neuroblasts. Those neuroblasts then enter the rostral migratory stream and move away from the SVZ. This approach allows eliminating one model parameter (transformation rate into neuroblasts) while maintaining the total number of progenitor division rounds.

\begin{figure}
	\centering
	\begin{subfigure}[b]{.48\textwidth}
		\centering
		\subcaption{\label{fig:modelStem}}
		\begin{tikzpicture}[baseline=0pt,node distance = 12ex, auto, scale=.65, transform shape,line width=.5pt]
			\node [quicell] (qS) {qNSC};
			\node [cyccell, below of=qS] (S) {aNSC}
			edge [<-] node {$r$} (qS);
			\node [decision, below of=S] (qquiact) {?}
			edge [<-] node {$p^\text{stem}$} (S);
			\begin{scope}[node distance=100pt and 100pt]
			\node [divisiontype, below left of=qquiact] (symStem) {$\text{aNSC}\rightarrow 2\times\text{qNSC}$}
			edge [<-] node {$b$} (qquiact);
			\node [divisiontype, below right of=qquiact] (symProg) {$\text{aNSC}\rightarrow 2\times\text{prog}_n$}
			edge [<-] node[auto,swap] {$1-b$} (qquiact);
			\end{scope}
		\end{tikzpicture}
	\end{subfigure}
	\begin{subfigure}[b]{.48\textwidth}
		\centering
		\subcaption{\label{fig:modelProg}}
		\begin{tikzpicture}[baseline=0pt,node distance = 10ex, auto, scale=.67, transform shape,line width=.5pt]
			\node [cyccell] (progn) {$\text{prog}_n$};
			\node [divisiontype, right of=progn,xshift=15ex] (divprog1) {$\text{prog}_n\rightarrow 2\times \text{prog}_{n-1}$}
			edge [<-] node {$p^\text{prog}$} (progn);
			
			\node [cyccell, below of=progn,yshift=-2ex] (progk) {$\text{prog}_{k}$};
			\node [divisiontype, right of=progk,xshift=15ex] (divprog2) {$\text{prog}_{k}\rightarrow 2\times \text{prog}_{k-2}$}
			edge [<-] node {$p^\text{prog}$} (progk);
			\node [below right of=progk,xshift=5ex] {$\vdots$};
			
			\node [cyccell, below of=progk,yshift=-2ex] (prog1) {$\text{prog}_{1}$};
			\node [divisiontype, right of=prog1,xshift=15ex] (divprog3) {$\text{prog}_{1}\rightarrow 2\times \text{prog}_{0}$}
			edge [<-] node {$p^\text{prog}$} (prog1);
			
			\node [cyccell, below of=prog1,yshift=-2ex] (prog0) {$\text{prog}_{0}$};
			\node [divisiontype, right of=prog0,xshift=15ex] (divprog4) {$\text{prog}_{0}\rightarrow 2\times \text{nblast}$}
			edge [<-] node {$p^\text{prog}$} (prog0);
		\end{tikzpicture}
	\end{subfigure}
\caption{
	\textbf{Graphical representation of the proposed model}\\
	 \subref{fig:modelStem} Quiescent neural stem cells (qNSCs) can enter the cell cycle to become activated NSCs (aNSC), which in turn divide to either produce two qNSCs or two progenitors with the capacity of $n$ symmetric self-renewing division ($\textsf{prog}_n$). \subref{fig:modelProg} Progenitors with capacity of $k$ symmetric self-renewing divisions ($k=1,\ldots, n$) divide into two progenitors with capacity of $k-1$ symmetric self-renewing divisions and progenitors without this capacity ($\textsf{prog}_0$) symmetrically divide into two neuroblasts.
}
\label{fig:model}
\end{figure}

\section{Quantification}
The cell cycle dynamics of NSCs and progenitors have been investigated in the study of \textcite{Ponti:2013}. Average cell cycle and S-phase lengths of both cell types have been measured as
$t_c^\text{stem}=\SI{17.5}{\hour}$, 
$t_s^\text{stem}=\SI{4.4}{\hour}$, 
$t_c^\text{prog}=\SI{20.5}{\hour}$ and
$t_s^\text{prog}=\SI{13.5}{\hour}$. To compute the proliferation rate of NSCs and progenitors, we interpret cell cycle lengths as doubling times of the corresponding exponential growth processes, leading to
\[p^\text{stem}=\frac{\log(2)}{t_c^\text{stem}} ~~~\text{and}~~~ p^\text{prog}=\frac{\log(2)}{t_c^\text{prog}}.\]
The remaining parameters $r$ and $b$ are estimated by fitting the model to the data, consisting of measurements of the number of NSCs, the fraction of aNSCs among all NSCs and the fraction of active label retaining (Ki67$^+$BrdU$^+$) cells on all label retaining (BrdU$^+$) cells. For a mathematical explanation of how to simulate the fraction of active label retaining cells, we refer the reader to section \ref{sec:frac}. Estimating model parameters results in a good fit to the data (Figure \ref{fig:modelfit}). 
\begin{figure}[htbp]
\begin{subfigure}[b]{.31\textwidth}
\centering
	\subcaption{\label{fig:noAgingStem}}
	\includegraphics[width=\textwidth]{include/noAgingStem.pdf}
\end{subfigure}
\hfill
\begin{subfigure}[b]{.31\textwidth}
\centering
	\subcaption{\label{fig:noAgingAct}}
	\includegraphics[width=\textwidth]{include/noAgingAct.pdf}
\end{subfigure}
\hfill
\begin{subfigure}[b]{.31\textwidth}
\centering
	\subcaption{\label{fig:noAgingLrc}}
	\includegraphics[width=\textwidth]{include/noAgingLrc.pdf}
\end{subfigure}
\caption{Fit of neurogenesis model \eqref{eq:model} to the data. Estimated parameters are $r=\SI{0.207}{\per\day}$ and $b=0.487$.}
\label{fig:modelfit}
\end{figure}

\section{Aging}
In the case of model \eqref{eq:model} with constant parameters, the ratio of qNSCs to aNSCs remains constant, since the quasi steady state $\lim_{t\to\infty}\frac{\textsf{qNSC}(t)}{\textsf{aNSC}(t)}$ is rapidly achieve. Hence, the predicted fraction of aNSCs among NSCs (Figure \ref{fig:noAgingAct}) and the fraction of active cells among label retaining cells (Figure \ref{fig:noAgingLrc}) remains constant in time. Since both fractions decline during aging, we asked which mechanisms can explain this decline.

Model \eqref{eq:model} consists of 3 NSC related parameters, $r$, $b$ and $p^\text{stem}$. In order to investigate how the dynamics of NSCs change during aging, we consider three scenarios, each related to a time-depended change of one of the three parameters. The first scenario, \textit{increasing quiescence}, assumes that NSCs spend progressively longer time in the qNSCs stage during aging. In that case, the relative number of aNSCs on all NSCs declines, which could explain the data displayed in Figures \ref{fig:noAgingAct} and \ref{fig:noAgingLrc}. The second scenario, \textit{increasing self-renewal}, assumes that during aging, a higher fraction of aNSCs divisions result in two qNSCs rather than two progenitors. The resulting increased production of qNSCs leads to a higher fraction of qNSCs on all NSCs and, as in the case of the first scenario, a decreased fraction of aNSCs on all NSCs. The last mechanism, \textit{cell cycle lengthening}, assumes that aNSCs take progressively longer to complete their cell cycle. This also results in a higher fraction of aNSCs on all NSCs, since the time spent in the aNSCs phase increases compared to the time spent in quiescence.

The increasing quiescence scenario is modeled with a declining activation rate,
\[
r(t)=r_\text{max}e^{-\beta_r t},
\]
increasing self-renewal is modeled with
\[
b(t)=\frac{1}{2}\left(1+e^{-\beta_b t}(2b_\text{min}-1)\right)
\]
and cell cycle lengthening is implemented with
\[
p^\text{stem}(t)=p^\text{stem}_\text{min}e^{-\beta_p t}.
\]
The reason for the specific choice of the function $b(t)$ is that NSCs are observed to decline during aging, i.e. $b(t)\leq 1/2$ must hold. For $b_\text{min}\leq1/2$, this can always be achieved in that way. Estimating model parameters results in the fits displayed in Figure \ref{fig:aging}, indicating that only the increasing quiescence can explain the aging effect observed in the data.

\begin{figure}[htbp]
\begin{subfigure}{.9\textwidth}
\centering
	\subcaption{\label{fig:decAct}}
	\includegraphics[width=.31\textwidth]{include/decActStem.pdf}\hfill
	\includegraphics[width=.31\textwidth]{include/decActAct.pdf}\hfill
	\includegraphics[width=.31\textwidth]{include/decActLrc.pdf}
\end{subfigure}\\
\begin{subfigure}{.9\textwidth}
\centering
	\subcaption{\label{fig:incSelf}}
	\includegraphics[width=.31\textwidth]{include/incSelfStem.pdf}\hfill
	\includegraphics[width=.31\textwidth]{include/incSelfAct.pdf}\hfill
	\includegraphics[width=.31\textwidth]{include/incSelfLrc.pdf}
\end{subfigure}

\begin{subfigure}{.9\textwidth}
\centering
	\subcaption{\label{fig:decProl}}
	\includegraphics[width=.31\textwidth]{include/decProlStem.pdf}\hfill
	\includegraphics[width=.31\textwidth]{include/decProlAct.pdf}\hfill
	\includegraphics[width=.31\textwidth]{include/decProlLrc.pdf}
\end{subfigure}
\caption{Fit of neurogenesis model \eqref{eq:model} to the data, assuming an increasing quiescence \subref{fig:decAct}, increasing self-renewal \subref{fig:incSelf} and cell cycle lengthening \subref{fig:decProl}.}
\label{fig:aging}
\end{figure}

To substantiate our observations that only increasing quiescence can explain the data, we make use of model selection theory by computing Akaike information criterion (AIC) weights for each of the discussed scenarios (Table \ref{tab:agingModels}).
The recommendation is that the level of empirical support of a certain model is substantial if $0\leq\Delta\leq 2$, considerably less, if $4\leq\Delta\leq7$ and essentially none, if $\Delta>10$ holds \autocite{Burnham:2002}. Thus, model selection theory also suggests that only the increasing quiescence scenario explains the data. To further characterize the increasing quiescence scenario, we can compute the mean time a NSC spends in quiescence during aging. The calculation is based on a corresponding stochastic view of the differential equation model by means of the Gillespie method \autocite{Gillespie:1977} and the property that a random variable, exponentially distributed with parameter $r$, has mean $1/r$. The resulting mean time in quiescence, $1/r(t)$, is displayed in Figure \ref{fig:meanQui}.

\begin{table}
\centering
	\caption{Parameters estimated during the analysis of different mechanisms to explain data. AICc is the small sample size corrected Akaike information criterion and $\Delta$AICc the corresponding Akaike weight, i.e.\ the difference to the smallest AICc value.}
	\begin{tabular}{cp{19ex}ccc} 
		\toprule Mechanism & Parameters & $R^2$ & AICc & $\Delta$AICc \\ 
		
		\midrule Increasing quiescence &
		$r_\text{min}=0.417\,\mathrm{d}^{-1}$\newline
		$\beta_r=0.00148\,\mathrm{d}^{-1}$\newline
		$b=0.491$
		& $0.994$ & $64.2$ & $0$\\
		
		\midrule Increasing self-renewal &
		$r=0.0794\,\mathrm{d}^{-1}$\newline
		$b_\text{min}=6.6\times 10^{-5}$\newline
		$\beta_b=0.0165\,\mathrm{d}^{-1}$
		& $0.968$ & $77.6$ & $13.4$\\
			
		\midrule Cell cycle lengthening &
		$p_\text{max}=\,0.951\mathrm{d}^{-1}$\newline
		$\beta_p=\,6.4\times 10^{-9}\mathrm{d}^{-1}$\newline
		$r=\,0.207\mathrm{d}^{-1}$\newline
		$b=0.487\,$
		& $0.965$ & $78.4$ & $14.2$\\
		\bottomrule
	\end{tabular}
	\label{tab:agingModels}
\end{table}

\begin{figure}
\centering
\includegraphics[width=.4\textwidth]{include/meanQui.pdf}
\caption{Predicted mean time a NSC spends in the quiescent phase depending on the age of the animal. The prediction is based on the increasing quiescence scenario displayed in Table \ref{tab:agingModels}.}
\label{fig:meanQui}
\end{figure}

\section{Fraction of Active Label Retaining Cells}\label{sec:frac}
In order to simulate the fraction of active label retaining cells, mathematical insights have to be utilized: The protocol used for measuring active label retaining cells was administration of BrdU in drinking water for two weeks followed by a two week waiting period before animal sacrifice (Figure \ref{fig:lrcExp}). 

\begin{figure}
\centering
\begin{tikzpicture}
	\tikzstyle{every node}=[font=\scriptsize]
		\draw [->,ultra thick] (0,0) -- (11,0);
		\draw (0,-.3) -- (0,.3);
		\node [text width=6ex, text centered] at (0,.9) {BrdU\\start};
		\node [text width=4ex, text centered] at (0,-.7) {$\tstart$};
		\draw (5.1,-.3) -- (5.1,.3);
		\node [text width=4ex, text centered] at (5.1,.9) {BrdU\\stop};
		\node [text width=4ex, text centered] at (5.1,-.7) {$\tstop$};
		\draw [<->] (0,.4) -- (5.1,.4);
		\node [text width=6ex, text centered] at (2.55,.7) {2w};
		\draw (10.4,-.3) -- (10.4,.3);
		\node [text width=4ex, text centered] at (10.4,-.7) {$\sac$};
		\draw [<->] (5.1,.4) -- (10.4,.4);
		\node [text width=6ex, text centered] at (7.55,.7) {2w};
		\node at (10.8,.4) {$t$};
		\node [text width=4ex, text centered] at (10.2,.9) {sacrifice};
\end{tikzpicture}
\caption{Experimental protocol for the analysis of active label retaining cells. Animals had access to BrdU-diluted drinking water for two weeks. Two weeks after stop of BrdU administration, animals were sacrificed and label retaining cells analyzed.}
\label{fig:lrcExp}
\end{figure}

At the time of sacrifice, the fraction of Ki67$^+$BrdU$^+$ cells among all BrdU$^+$ cells is comprised of aNSCs and progenitors:
\begin{equation}\label{eq:ki67}
\frac{\ki\br\ce}{\br\ce}(t=\sac)=\frac{\br\an+\br\pr}{\br\qn+\br\an+\br\pr}(t=\sac).
\end{equation}
Moreover, the ratio of aNSCs to qNSCs at the time of sacrifice mirrors the ratio of $\br$ aNSCs to $\br$ qNSCs at that time, since during the two week waiting period between BrdU stop and sacrifice, a quasi steady-state among $\br$ cells is achieved:
\begin{equation}\label{eq:aqFrac}
\frac{\br\an}{\br\qn}(t=\sac)=\frac{\an}{\qn}(t=\sac).
\end{equation}
Putting \eqref{eq:ki67} and \eqref{eq:aqFrac} together, we obtain
\begin{equation}\label{eq:progQnsc}
\frac{\ki\br\ce}{\br\ce}(t=\sac)=\frac{\frac{\an}{\qn}+\frac{\br\pr}{\br\qn}}{1+\frac{\an}{\qn}+\frac{\br\pr}{\br\qn}}(t=\sac).
\end{equation}
Thus, in order to compute the fraction of $\ki$ cells among all $\br$ cells at the time of sacrifice, we need to compute $\frac{\br\pr}{\br\qn}(t=\sac)$, since the ratio $\frac{\an}{\qn}(t=\sac)$ can be obtained from the solution of the differential equation model \eqref{eq:model}. The ratio $\frac{\br\pr}{\br\qn}(t=\sac)$ cannot be obtained from the differential equation. The reason is that any $\textsl{prog}_n$ cell being produced prior to the time point $t=\sac-(n+2)t_c^\text{prog}$ would have already become a neuroblast at $t=\sac$, but according to the differential equation, a large number of $\textsl{prog}_n$ cells being born prior to $t=\sac-(n+2)t_c^\text{prog}$ would remain at $t=\sac$. Such individual based behavior like the deterministic conversion of one cell type to another after a fixed amount of time cannot be simulated with population dynamics. However, the ratio $\frac{\br\pr}{\br\qn}(t=\sac)$ can be evaluated as follows:

$\br$ progenitors at the time of sacrifice can only originate from $\br$ qNSCs that exist at the time where BrdU is stopped in the drinking water, because any qNSCs becoming activated at an earlier time would lead to progenitors that, at the time of sacrifice, would already have become neuroblasts that migrated away from the SVZ. If a qNSCs existing at the time of BrdU stop becomes an aNSCs at a time between $t=\sac-(n+1-k)t_c^{\textsl{prog}}-t_c^{\textsl{stem}}$ and $t=\sac-(n-k)t_c^{\textsl{prog}}-t_c^{\textsl{stem}}$ \;($k=0,\ldots,n$), then one $t_c^\text{stem}$ later, this aNSC gives rise to two $\text{prog}_{0}$ cells with probability $1-b$ and a further $(n-k)$ $t_c^\text{prog}$ later, $2^{n-k}$ $\textsl{prog}_k$ progenitors exist at a time between $t=\sac-t_c^{\textsl{prog}}$ and $t=\sac$. This chain of reasoning can be summarized as follows:
\begin{itemize}
\item $(\br\qn\to\br\an)@[\sac-(n+1-k)t_c^{\textsl{prog}}-t_c^{\textsl{stem}},\sac-(n-k)t_c^{\textsl{prog}}-t_c^{\textsl{stem}}]$
\item[$\Rightarrow$] $(\text{birth~of~} 2\textsl{prog}_0)@[\sac-(n+1-k)t_c^{\textsl{prog}},\sac-(n-k)t_c^{\textsl{prog}}] \text{~w.~prob.~} 1-b$
\item[$\Rightarrow$] $(\text{birth~of~} 2^{n-k}\textsl{prog}_k)@[\sac-t_c^{\textsl{prog}},\sac] \text{~w.~prob.~} 1-b$
\end{itemize}
Let $\mathcal{P}(x)$ denote the probability of event $x$, it holds that
{\scriptsize
	\[
	\begin{array}{cl}
	&\mathcal{P}\bigg((\br\qn\to\br\an)@[\sac-(n+1-k)t_c^{\textsl{prog}}-t_c^{\textsl{stem}},\;\sac-(n-k)t_c^{\textsl{prog}}-t_c^{\textsl{stem}}]\bigg)\\
	&= \mathcal{P}\bigg((\br\qn\to\br\an)@[\tstop+\SI{14}{\day}-(n+1-k)t_c^{\textsl{prog}}-t_c^{\textsl{stem}},\;\tstop+\SI{14}{\day}-(n-k)t_c^{\textsl{prog}}-t_c^{\textsl{stem}}]\bigg)\\
	&= e^{-r\left(\SI{14}{\day}-(n-k)t_c^{\textsl{prog}}-t_c^{\textsl{stem}}\right)}\left(e^{r t_c^{\textsl{prog}}}-1\right).
	\end{array}
	\]
}%
The first equality holds since sacrifice occurred two weeks after BrdU stop, i.e. $\sac=\tstop+\SI{14}{\day}$ and the second because in the Gillespie sense \autocite{Gillespie:1977}, the time until a qNSCs becomes an aNSCs is exponentially distributed with parameter $r$ and it holds that
\[
\mathcal{P}(X\in[t_1,t_2]\mid X\sim \exp(r)) = e^{-rt_1}-e^{-rt_2}.
\]
Taken together, we have that
\[
\frac{\br\pr(t=\sac)}{\br\qn(t=\tstop)}=(1-b)\sum_{k=0}^n 2^{n-k} e^{-r\left(\SI{14}{\day}-(n-k)t_c^{\textsl{prog}}-t_c^{\textsl{stem}}\right)}\left(e^{r t_c^{\textsl{prog}}}-1\right)
\]
and thus
{\footnotesize
\[
\frac{\br\pr(t=\sac)}{\br\qn(t=\sac)}=\frac{\br\qn(t=\tstop)}{\br\qn(t=\sac)}(1-b)\sum_{k=0}^n 2^{n-k} e^{-r\left(\SI{14}{\day}-(n-k)t_c^{\textsl{prog}}-t_c^{\textsl{stem}}\right)}\left(e^{r t_c^{\textsl{prog}}}-1\right).
\]
}
\printbibliography

\end{document}